<?php

/**
 * @file
 * Delayed queue functionality.
 *
 *   Uses two queues, one to queue delayed items, ab(using) the created time
 *   for the time to move the queue to the ready queue where jobs are run from.
 *
 *   An alternative would be to lock the jobs until the time they should be
 *   available, but the use case required knowing which jobs are actually
 *   locked for processing and which jobs are not available for processing.
 *
 *   Another alternative would be to extend advancedqueue which has an
 *   additional field for status.
 */

/**
 * Default extension of SystemQueue.
 */
class DelayQueue extends AdvancedQueue {
  /**
   * Add item to queue with a delay.
   *
   * @param mixed|DelayQueueJob $data
   *   Arbitary data to be associated with the new task in the queue.
   *
   * @return bool
   *   TRUE if the item was successfully created and was (best effort) added
   *   to the queue, otherwise FALSE. We don't guarantee the item was
   *   committed to disk etc, but as far as we know, the item is now in the
   *   queue.
   */
  public function createItem($data) {
    if (! is_a($data, 'DelayQueueJob')) {
      $data = new DelayQueueJob($data);
    }
    $query = db_insert('advancedqueue')
      ->fields(array('name' => $this->name) + $data->prepare());
    return (bool) $query->execute();
  }

  public function claimItem($lease_time = 30) {
    $item = parent::claimItem($lease_time);
    // Kept check from previous verison, all items now DelayQueueJob so
    // could be removed.
    if ($item && is_a($item->data, 'DelayQueueJob')) {
      $item->data->item_id = $item->item_id;
      $item = $item->data;
    }
    return $item;
  }

  /**
   * Release an item that the worker could not process, so another
   * worker can come in and process it.
   *
   * If the item should be delayed again reset the $item->enqueued time
   * before releasing it.
   *
   * @param $item
   *
   * @return bool
   *   Result of database update.
   */
  public function releaseItem($item) {
    $update = db_update('advancedqueue')
      ->fields(array('name' => $this->name) + $item->prepare())
      ->condition('item_id', $item->item_id);
    return $update->execute();
  }

  /**
   * Retrieve the number of items queue with delayed start.
   *
   * @return int
   *   An integer estimate of the number of items in the queues.
   */
  public function numberOfDelayedItems() {
    return db_query('SELECT COUNT(item_id) FROM {advancedqueue} WHERE name = :name AND STATUS = ' . DELAYQUEUE_STATUS_DELAYED . ' AND expire > :time', array(':name' => $this->name, ':time' => time()))->fetchField();
  }
}

/**
 * Delayed queue item.
 *
 * Use this class to create an item to go into the delayed queue.
 */
class DelayQueueJob {
  /**
   * Queue name
   *
   * @var string
   */
  public $queueName;

  /**
   * Advanced Queue Primary Key.
   *
   * @var int
   */
  public $item_id;

  /**
   * The user to which the item belongs.
   *
   * @var int
   */
  public $uid;

  /**
   * The title of this item.
   *
   * @var string
   */
  public $title;

  /**
   * Delay in seconds before job should move into ready queue.
   *
   * @var int
   */
  public $delay;

  /**
   * Timestamp of original job creation.
   *
   * @var int
   */
  public $enqueued;

  /**
   * Arbitary data to store in the queue.
   *
   * @var mixed
   */
  public $data;

  /**
   * Create new delayed queue item.
   *
   * @param mixed $data
   *   Data for item.
   * @param string $title
   *   Optional. The title of this item.
   * @param int $uid
   *   Optional. The usert to which the item belongs.
   * @param int $delay
   *   Delay in seconds.
   * @param bool $set_enqueued
   *   If the created, queued, time should be set on creation of the object.
   */
  public function __construct($data, $title = 'Unnamed item', $uid = NULL, $delay = 0, $set_enqueued = TRUE) {
    $this->title = $title;
    $this->uid = $uid === NULL ? $GLOBALS['user']->uid : $uid;
    $this->data = $data;
    $this->delay = $delay;
    if ($set_enqueued) {
      $this->enqueued = time();
    }
  }

  /**
   * Prepare item to be inserted, or updated, in queue.
   *
   * @return array
   *   Keyed array of fields and values for database.
   */
  public function prepare() {
    if (empty ($this->enqueued)) {
      $this->enqueued = time();
    }
    if ($this->delay + $this->enqueued > time()) {
      $delay = $this->delay + $this->enqueued;
    }
    else {
      $delay = 0;
    }
    return array(
      'name' => $this->queueName,
      'uid' => $this->uid,
      'title' => $this->title,
      'data' => serialize($this),
      'created' => $this->enqueued,
      'status' => $delay ? DELAYQUEUE_STATUS_DELAYED :   ADVANCEDQUEUE_STATUS_QUEUED,
      'expire' => $delay,
    );
  }
}
